/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.xliff;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;

@RunWith(JUnit4.class)
public class XLIFFFilterSDLPropTest {

	private FileLocation location;
	private IFilterConfigurationMapper fcMapper;
    private LocaleId locENUS = LocaleId.fromString("en-US");
    private LocaleId locFRFR = LocaleId.fromString("fr-FR");

    @Before
    public void setUp() {
		location = FileLocation.fromClass(XLIFFFilterTest.class);
    	fcMapper = new FilterConfigurationMapper();
        fcMapper.addConfigurations("net.sf.okapi.filters.xliff.XLIFFFilter");
    }

    @Test
    public void testSegmentProperties () {
    	try ( IFilter filter = fcMapper.createFilter("okf_xliff-sdl") ) {
    		filter.open(new RawDocument(location.in("/test.txt_en-US_fr-FR.sdlxliff").asUri(),
    			"UTF-8", locENUS, locFRFR, "okf_xliff-sdl"));
    		while ( filter.hasNext() ) {
    			Event event = filter.next();
    			if ( !event.isTextUnit() ) continue;
    			ITextUnit tu = event.getTextUnit();
    			
    			// Check we still have the properties on the text container
    			// And it is the values for the last segment
    			TextContainer tc = tu.getTarget(locFRFR);
    			assertEquals("tm", tc.getProperty(SdlXliffSkeletonWriter.PROP_SDL_ORIGIN).getValue());
    			assertEquals("Draft", tc.getProperty(SdlXliffSkeletonWriter.PROP_SDL_CONF).getValue());

    			// Check the properties are also on each segment
    			ISegments segs = tc.getSegments();
    			assertEquals(3, segs.count());
    			Segment seg = segs.get(0);
    			assertEquals("interactive", seg.getProperty(SdlXliffSkeletonWriter.PROP_SDL_ORIGIN).getValue());
    			assertEquals("Translated", seg.getProperty(SdlXliffSkeletonWriter.PROP_SDL_CONF).getValue());
    			seg = segs.get(1);
    			assertEquals("mt", seg.getProperty(SdlXliffSkeletonWriter.PROP_SDL_ORIGIN).getValue());
    			assertEquals("Translated", seg.getProperty(SdlXliffSkeletonWriter.PROP_SDL_CONF).getValue());
    			seg = segs.get(2);
    			assertEquals("tm", seg.getProperty(SdlXliffSkeletonWriter.PROP_SDL_ORIGIN).getValue());
    			assertEquals("Draft", seg.getProperty(SdlXliffSkeletonWriter.PROP_SDL_CONF).getValue());
    		}
    	}
    }

    @Test
    @Ignore // Currently the writer uses the values in the text container to update all segments
    public void testSegmentPropertiesOutput () {
    	try ( IFilter filter = fcMapper.createFilter("okf_xliff-sdl") ) {
    		filter.open(new RawDocument(location.in("/test.txt_en-US_fr-FR.sdlxliff").asUri(),
    			"UTF-8", locENUS, locFRFR, "okf_xliff-sdl"));
    		
    		IFilterWriter fw = filter.createFilterWriter();
    		fw.setOutput(location.in("/test.txt_en-US_fr-FR.OUT.sdlxliff").toString());
    		fw.setOptions(locFRFR, "UTF-8");
    		
    		while ( filter.hasNext() ) {
    			Event event = filter.next();
    			if ( !event.isTextUnit() ) {
    				fw.handleEvent(event);
    				continue;
    			}
    			ITextUnit tu = event.getTextUnit();
    			
    			// Check we still have the properties on the text container
    			// And it is the values for the last segment
    			TextContainer tc = tu.getTarget(locFRFR);
    			Property prop = tc.getProperty(SdlXliffSkeletonWriter.PROP_SDL_ORIGIN);
    			assertEquals("tm", prop.getValue());
    			prop.setValue("xyz");

    			assertEquals("Draft", tc.getProperty(SdlXliffSkeletonWriter.PROP_SDL_CONF).getValue());
    			
    			fw.handleEvent(event);
    		}
    		fw.close();
    	}
    }
}
