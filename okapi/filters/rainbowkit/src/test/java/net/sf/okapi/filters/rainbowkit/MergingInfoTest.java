package net.sf.okapi.filters.rainbowkit;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;

@RunWith(DataProviderRunner.class)
public class MergingInfoTest {

	@DataProvider
	public static Object[][] testSimpleWriteDataProvider() {
		return new Object[][] {
				{1, "et", "inpPath", "fi", "#v1\nfp", "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc",
				"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\">"
						+ "<filterParameters>#v1\nfp</filterParameters><subfilterParameters>#v1\nsfp</subfilterParameters></test>"},

				{1, "et", "inpPath", "fi", null, "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc",
						"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\">"
								+ "<subfilterParameters>#v1\nsfp</subfilterParameters></test>"},

				{1, "et", "inpPath", "fi", "#v1\\nfp", "sfi", null, "inEnc", "outPath", "outEnc",
						"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\">"
								+ "<filterParameters>#v1\\nfp</filterParameters></test>"},

				{1, "et", "inpPath", "fi", null, "sfi", null, "inEnc", "outPath", "outEnc",
						"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\" />"}
		};
	}

	@Test
	@UseDataProvider("testSimpleWriteDataProvider")
	public void testSimpleWrite(int docId,
								String extractionType,
								String relativeInputPath,
								String filterId,
								String filterParameters,
								String subfilterId,
								String subfilterParameters,
								String inputEncoding,
								String relativeTargetPath,
								String targetEncoding,
								String expectedResult) {
		MergingInfo info = new MergingInfo(docId, extractionType, relativeInputPath, filterId, filterParameters, subfilterId, subfilterParameters,
				inputEncoding, relativeTargetPath, targetEncoding);

		Assert.assertThat(info.writeToXML("test", false), equalTo(expectedResult));
	}

	@DataProvider
	public static Object[][] testSimpleWriteBase64DataProvider() {
		return new Object[][] {
				{1, "et", "inpPath", "fi", "#v1\nfp", "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc",
						"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\">"
								+ "<filterParameters>I3YxCmZw</filterParameters><subfilterParameters>I3YxCnNmcA==</subfilterParameters></test>"},

				{1, "et", "inpPath", "fi", null, "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc",
						"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\">"
								+ "<subfilterParameters>I3YxCnNmcA==</subfilterParameters></test>"},

				{1, "et", "inpPath", "fi", "#v1\nfp", "sfi", null, "inEnc", "outPath", "outEnc",
						"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\">"
								+ "<filterParameters>I3YxCmZw</filterParameters></test>"},

				{1, "et", "inpPath", "fi", null, "sfi", null, "inEnc", "outPath", "outEnc",
						"<test xml:space=\"preserve\" docId=\"1\" extractionType=\"et\" relativeInputPath=\"inpPath\" filterId=\"fi\" subfilterId=\"sfi\" inputEncoding=\"inEnc\" relativeTargetPath=\"outPath\" targetEncoding=\"outEnc\" selected=\"1\" />"}
		};
	}

	@Test
	@UseDataProvider("testSimpleWriteBase64DataProvider")
	public void testSimpleWriteBase64(int docId,
									  String extractionType,
									  String relativeInputPath,
									  String filterId,
									  String filterParameters,
									  String subfilterId,
									  String subfilterParameters,
									  String inputEncoding,
									  String relativeTargetPath,
									  String targetEncoding,
									  String expectedResult) {
		MergingInfo info = new MergingInfo(docId, extractionType, relativeInputPath, filterId, filterParameters, subfilterId, subfilterParameters,
				inputEncoding, relativeTargetPath, targetEncoding);

		Assert.assertThat(info.writeToXML("test", true), equalTo(expectedResult));
	}

	@DataProvider
	public static Object[][] testSimpleWriteAndReadDataProvider() {
		return new Object[][] {
				{1, "et", "inpPath", "fi", "#v1\nfp", "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc", false},
				{1, "et", "inpPath", "fi", null, "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc", false},
				{1, "et", "inpPath", "fi", "#v1\nfp", "sfi", null, "inEnc", "outPath", "outEnc", false},
				{1, "et", "inpPath", "fi", null, "sfi", null, "inEnc", "outPath", "outEnc", false},

				{1, "et", "inpPath", "fi", "#v1\nfp", "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc", true},
				{1, "et", "inpPath", "fi", null, "sfi", "#v1\nsfp", "inEnc", "outPath", "outEnc", true},
				{1, "et", "inpPath", "fi", "#v1\nfp", "sfi", null, "inEnc", "outPath", "outEnc", true},
				{1, "et", "inpPath", "fi", null, "sfi", null, "inEnc", "outPath", "outEnc", true}
		};
	}

	@Test
	@UseDataProvider("testSimpleWriteAndReadDataProvider")
	public void testSimpleWriteAndRead(int docId,
									   String extractionType,
									   String relativeInputPath,
									   String filterId,
									   String filterParameters,
									   String subfilterId,
									   String subfilterParameters,
									   String inputEncoding,
									   String relativeTargetPath,
									   String targetEncoding,
									   boolean encodeToBase64) throws SAXException, IOException, ParserConfigurationException {
		MergingInfo info1 = new MergingInfo(docId, extractionType, relativeInputPath, filterId, filterParameters, subfilterId, subfilterParameters,
				inputEncoding, relativeTargetPath, targetEncoding);
		String res = info1.writeToXML("test", encodeToBase64);
		
		DocumentBuilderFactory Fact = DocumentBuilderFactory.newInstance();
		InputSource is = new InputSource(new StringReader(res));
		Document doc = Fact.newDocumentBuilder().parse(is);
		Element elem = doc.getDocumentElement();
		MergingInfo info2 = MergingInfo.readFromXML(elem);

		assertEquals(res, info2.writeToXML("test", encodeToBase64));
		assertEquals(info1.getDocId(), info2.getDocId());
		assertEquals(info1.getFilterId(), info2.getFilterId());
		assertEquals(info1.getSubfilterId(), info2.getSubfilterId());
		assertEquals(info1.getExtractionType(), info2.getExtractionType());
		assertEquals(info1.getRelativeInputPath(), info2.getRelativeInputPath());
		assertEquals(info1.getFilterParameters(), info2.getFilterParameters());
		assertEquals(info1.getSubfilterParameters(), info2.getSubfilterParameters());
		assertEquals(info1.getInputEncoding(), info2.getInputEncoding());
		assertEquals(info1.getTargetEncoding(), info2.getTargetEncoding());
		assertEquals(info1.getRelativeTargetPath(), info2.getRelativeTargetPath());
	}
}
