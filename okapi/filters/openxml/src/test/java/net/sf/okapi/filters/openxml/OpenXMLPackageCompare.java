package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import net.sf.okapi.common.IFileCompare;
import net.sf.okapi.filters.openxml.OpenXMLPackageDiffer.Difference;

/**
 * {@link IFileCompare} wrapper for {@link OpenXMLPackageDiffer}.
 */
public class OpenXMLPackageCompare implements IFileCompare {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Override
	public boolean filesExactlyTheSame(String outputFilePath, String goldFilePath) {
		Path outputPath = Paths.get(outputFilePath);
		Path goldPath = Paths.get(goldFilePath);
		try (InputStream outputIs = Files.newInputStream(outputPath);
				InputStream goldIs = Files.newInputStream(goldPath)) {
			OpenXMLPackageDiffer differ = new OpenXMLPackageDiffer(goldIs, outputIs);
			boolean rv = differ.isIdentical();
			if (!rv) {
				dumpDifferences(outputFilePath, goldFilePath, differ.getDifferences());
			}
			return rv;
		}
		catch (IOException | SAXException e) {
			// We should just propagate this but I would need to fix some code in other
			// classes.
			throw new RuntimeException(e);
		}
	}

	private void dumpDifferences(String goldPath, String outputPath, List<Difference> differences) {
		LOGGER.warn("Gold: {}\nOutput: {}", goldPath, outputPath);
		for (OpenXMLPackageDiffer.Difference d : differences) {
			LOGGER.warn("+ {}", d.toString());
		}
	}
}