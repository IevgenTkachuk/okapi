package net.sf.okapi.common;

/**
 * Provides a file compare interface.
 */
public interface IFileCompare {
    boolean filesExactlyTheSame (String outputFilePath, String goldFilePath);
}
