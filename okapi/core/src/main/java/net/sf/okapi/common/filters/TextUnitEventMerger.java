package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.IDecoder;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.resource.TextUnitUtil;

import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import static net.sf.okapi.common.EventType.TEXT_UNIT;
import static net.sf.okapi.common.filters.CodeReferenceTranscoder.transcodeFromMaskedBase;
import static net.sf.okapi.common.filters.CodeReferenceTranscoder.transcodeToMaskedBase;

/**
 * Merges subfiltered events back into a single TextUnit.
 */
public class TextUnitEventMerger extends TextUnitEventHolder {

    private String parentId;
    private String parentName;
    private ISkeleton parentSkeleton;
    private List<Code> parentCodes;

    private SubFilterSkeletonWriter skeletonWriter;
    private IDecoder decoder;

    public TextUnitEventMerger(String parentId, String parentName, ISkeleton parentSkeleton, List<Code> parentCodes,
                               SubFilterSkeletonWriter skeletonWriter, IDecoder decoder) {
        super();

        this.parentId = parentId;
        this.parentName = parentName;
        this.parentSkeleton = parentSkeleton;
        this.parentCodes = parentCodes;

        this.skeletonWriter = skeletonWriter;
        this.decoder = decoder;
    }

    public TextUnitEventMerger(TextUnitEventMerger textUnitEventMerger) {
        super(textUnitEventMerger);

        parentId = textUnitEventMerger.parentId;
        parentName = textUnitEventMerger.parentName;
        parentSkeleton = textUnitEventMerger.parentSkeleton;
        parentCodes = TextUnitUtil.cloneCodes(textUnitEventMerger.parentCodes);

        skeletonWriter = new SubFilterSkeletonWriter(textUnitEventMerger.skeletonWriter);
        decoder = textUnitEventMerger.decoder;
    }

    /**
     * Gets the added text unit event.
     *
     * @return The added text unit event
     * @throws IllegalStateException if a text unit event is not found
     */
    @Override
    public Event getAddedTextUnitEvent() {
        for (Event event : events) {
            if (TEXT_UNIT == event.getEventType()) {
                return event;
            }
        }

        throw new IllegalStateException();
    }

    @Override
    public TextUnitEventMerger addEvent(Event event) {
        events.add(event);

        return this;
    }

    public TextUnitEventMerger replaceAddedTextUnitEvent(Event textUnitEvent) {
        ListIterator<Event> eventListIterator = events.listIterator();

        while (eventListIterator.hasNext()) {
            Event event = eventListIterator.next();

            if (TEXT_UNIT == event.getEventType()) {
                eventListIterator.set(getUpdateTextUnitEvent(event, textUnitEvent));
                return this;
            }
        }

        throw new IllegalStateException();
    }

    public TextUnitEventMerger replaceAll(List<Event> textUnitEvents) {

        ListIterator<Event> eventListIterator = events.listIterator();
        ListIterator<Event> textUnitEventIterator = textUnitEvents.listIterator();

        while (eventListIterator.hasNext()) {
            Event event = eventListIterator.next();
            if (TEXT_UNIT == event.getEventType()) {
                Event originalTextUnitEvent = null;
                while (textUnitEventIterator.hasNext()) {
                    originalTextUnitEvent = textUnitEventIterator.next();
                    if (originalTextUnitEvent.isTextUnit()) {
                        break;
                    }
                }
                if (originalTextUnitEvent != null) {
                    eventListIterator.set(getUpdateTextUnitEvent(event, originalTextUnitEvent));
                }
            }
        }
        return this;
    }

    private Event getUpdateTextUnitEvent(Event originalEvent, Event translatedEvent) {
        ITextUnit newTextUnit = originalEvent.getTextUnit().clone();
        ITextUnit translatedTextUnit = translatedEvent.getTextUnit();

        List<Code> originalCodes = newTextUnit.getSource().getAllCodes();

        for (LocaleId localeId : translatedTextUnit.getTargetLocales()) {
            newTextUnit.createTarget(localeId, false, IResource.COPY_ALL);

            String translatedCodedText = translatedTextUnit.getTarget(localeId).getContents().stream()
                    .map(TextFragment::getCodedText)
                    .collect(Collectors.joining());

            List<Code> translatedCodes = translatedTextUnit.getTarget(localeId).getAllCodes();

            TextFragment textFragment = getTextFragment(translatedCodedText, originalCodes, translatedCodes);

            newTextUnit.getTarget(localeId).setContent(textFragment);
        }

        translatedEvent.setResource(newTextUnit);

        return translatedEvent;
    }

    private TextFragment getTextFragment(String codedText, List<Code> originalCodes, List<Code> translatedCodes) {
        TextFragment textFragment = new TextFragment();

        char[] buf = codedText.toCharArray();
        for (int i = 0, codeIndex = 0; i < buf.length; i++) {

            if (!TextFragment.isMarker(buf[i])) {
                textFragment.append(buf[i]);
                continue;
            }

            Code code = getOriginalCodeByTranslatedCodeIdAndTagType(originalCodes, translatedCodes, codeIndex);

            if (null == code) {
                // there has been no original code found, converting to a string
                textFragment.append(translatedCodes.get(codeIndex).getOuterData());
                ++codeIndex;
				++i;
                continue;
            }
            ++codeIndex;
            ++i;
            textFragment.append(code);
        }

        return textFragment;
    }

    private Code getOriginalCodeByTranslatedCodeIdAndTagType(List<Code> originalCodes, List<Code> translatedCodes, int codeIndex) {
        int translatedCodeId = translatedCodes.get(codeIndex).getId();
        TagType translatedCodeTagType = translatedCodes.get(codeIndex).getTagType();

        for (Code originalCode : originalCodes) {
            if (translatedCodeId == originalCode.getId() && translatedCodeTagType == originalCode.getTagType()) {
                return originalCode.clone();
            }
        }

        return null;
    }

    public Event merge() {
        for (Event event : events) {
            EventType eventType = event.getEventType();

            switch (eventType) {
                case TEXT_UNIT:
                    ITextUnit textUnit = event.getTextUnit();
                    maskCodeReferences(textUnit, parentCodes);
                    skeletonWriter.processTextUnit(textUnit);
                    break;
                case DOCUMENT_PART:
                    skeletonWriter.processDocumentPart(event.getDocumentPart());
                    break;
                default:
                    throw new IllegalStateException(String.format("Could not handle %s event type", eventType.toString()));
            }
        }

        TextUnit textUnit = new TextUnit(parentId, skeletonWriter.getEncodedOutput(), false);
        textUnit.setName(parentName);
        textUnit.setSkeleton(parentSkeleton);

        unescapeSpecialCharacters(textUnit); // unescape after the skeleton writer
        unmaskCodeReferences(textUnit, parentCodes);

        return new Event(TEXT_UNIT, textUnit);
    }

    /**
     * Masks code references in a provided text unit.
     *
     * @param textUnit    A text unit
     * @param parentCodes Parent codes
     */
    private void maskCodeReferences(ITextUnit textUnit, List<Code> parentCodes) {
        List<TextFragment> maskedSourceTextFragments = textUnit.getSource().getContents().stream()
                .map(textFragment -> transcodeToMaskedBase(
                        textFragment.getCodedText(),
                        textFragment.getCodes(),
                        parentCodes))
                .collect(Collectors.toList());

        textUnit.getSource().setContent(maskedSourceTextFragments);

        for (LocaleId localeId : textUnit.getTargetLocales()) {
            List<TextFragment> maskedTargetTextFragments = textUnit.getTarget(localeId).getContents().stream()
                    .map(textFragment -> transcodeToMaskedBase(
                            textFragment.getCodedText(),
                            textFragment.getCodes(),
                            parentCodes))
                    .collect(Collectors.toList());

            textUnit.getTarget(localeId).setContent(maskedTargetTextFragments);
        }
    }

    /**
     * Unmasks code references in a provided text unit.
     *
     * @param textUnit    A text unit
     * @param parentCodes Parent codes
     */
    private void unmaskCodeReferences(ITextUnit textUnit, List<Code> parentCodes) {
        List<TextFragment> maskedSourceTextFragments = textUnit.getSource().getContents().stream()
                .map(textFragment -> transcodeFromMaskedBase(textFragment.getCodedText(),  parentCodes))
                .collect(Collectors.toList());

        textUnit.getSource().setContent(maskedSourceTextFragments);
    }

    /**
     * Unescapes special characters in a provided text unit.
     *
     * @param textUnit A text unit
     */
    private void unescapeSpecialCharacters(ITextUnit textUnit) {
        textUnit.getSource().getContents().forEach(textFragment -> {
            textFragment.setCodedText(decoder.decode(textFragment.getCodedText()));
        });

        for (LocaleId localeId : textUnit.getTargetLocales()) {
            textUnit.getTarget(localeId).getContents().forEach(textFragment -> {
                textFragment.setCodedText(decoder.decode(textFragment.getCodedText()));
            });
        }
    }
}

