package net.sf.okapi.common.encoder;

/**
 * Provides a decoder interface.
 */
public interface IDecoder {

    /**
     * Encodes a given text with this encoder.
     *
     * @param text The text to decode
     *
     * @return The decoded text
     */
    String decode(String text);
}
