package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.IDecoder;
import net.sf.okapi.common.resource.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static net.sf.okapi.common.filters.CodeReferenceTranscoder.transcodeFromMaskedBase;
import static net.sf.okapi.common.filters.CodeReferenceTranscoder.transcodeToMaskedBase;

/**
 * Class to perform the subfiltering of a single ITextunit.  This is used
 * both by <tt>SubfilteringStep</tt> and by <tt>Merger</tt> code that needs to
 * re-filter the original document to align and merge the translations.
 */
public class TextUnitEventSplitter {

    public static final int INITIAL_SECTION_INDEX = 1;

    private static final int LOCALE_INDEX = 0;

    private IFilter subfilter;
    private int subfilterIndex;
    private LocaleId sourceLocale;

    private List<LocaleId> targetLocales;
    private String outputEncoding;

    private TextUnitEventSplitter(IFilter subfilter, int subfilterIndex, LocaleId sourceLocale, List<LocaleId> targetLocales, String outputEncoding) {
        this.subfilter = subfilter;
        this.subfilterIndex = subfilterIndex;
        this.sourceLocale = sourceLocale;
        this.targetLocales = targetLocales;
        this.outputEncoding = outputEncoding;
    }

    /**
     * Apply the configured subfilter to the text unit and return a {@link MultiEvent}
     * containing all the events the subfilter produces.
     * @param textUnit TU to subfilter
     * @return MultiEvent containing the subfiltered events
     */
    public MultiEvent split(ITextUnit textUnit) {
        // XXX I'm passing null as the parentEncoder here.  This should be supported by
        // SubFilterSkeletonWriter, but it may not work quite right.  I may need to manually
        // re-escape things as a pre-merge step, because otherwise I won't know what my source
        // format was... will I?  Or could I suck it out of the startdoc?
        // I might be able to pull it from FILTER_CONFIGURATION_ID PP
        SubFilter subfilter = new SubFilter(this.subfilter, null, subfilterIndex++, textUnit.getId(), textUnit.getName());

        subfilter.open(new RawDocument(maskCodeReferences(textUnit.getSource().getCodedText()), sourceLocale));

        List<Event> subfilterEvents = new ArrayList<>();
        List<Code> parentCodes = textUnit.getSource().getAllCodes();

        while (subfilter.hasNext()) {
            Event subEvent = subfilter.next();

            if (subEvent.isTextUnit()) {
                unmaskCodeReferences(subEvent.getTextUnit(), parentCodes);
                subfilterEvents.add(subEvent);

            } else if (subEvent.isStartSubfilter()) {
                StartSubfilter startSubfilter = subEvent.getStartSubfilter();

                startSubfilter.setParentId(textUnit.getId());
                startSubfilter.setParentName(textUnit.getName());
                startSubfilter.setParentSkeleton(textUnit.getSkeleton());
                startSubfilter.setParentCodes(parentCodes);

                startSubfilter.setSkelWriter(getSkeletonWriter(startSubfilter));
                startSubfilter.setDecoder(getDecoder());

                subfilterEvents.add(subEvent);

            } else {
                subfilterEvents.add(subEvent);
            }
        }
        subfilter.close();

        return new MultiEvent(subfilterEvents);
    }

    /**
     * Masks code references in a provided text.
     *
     * @param codedText A text
     *
     * @return A text with masked code references
     */
    private String maskCodeReferences(String codedText) {
        return transcodeToMaskedBase(codedText);
    }

    /**
     * Unmasks code references in a provided text unit.
     *
     * @param textUnit      A text unit
     * @param originalCodes Original codes before masking
     */
    private void unmaskCodeReferences(ITextUnit textUnit, List<Code> originalCodes) {
        List<TextFragment> maskedSourceTextFragments = textUnit.getSource().getContents().stream()
                .map(textFragment -> transcodeFromMaskedBase(
                        textFragment.getCodedText(),
                        textFragment.getCodes(),
                        originalCodes))
                .collect(Collectors.toList());

        textUnit.getSource().setContent(maskedSourceTextFragments);
    }

    /**
     * Gets a skeleton writer.
     *
     * @param startSubfilter A start subfilter
     *
     * @return A skeleton writer
     */
    private SubFilterSkeletonWriter getSkeletonWriter(StartSubfilter startSubfilter) {
        return (SubFilterSkeletonWriter) startSubfilter.createSkeletonWriter(null, targetLocales.get(LOCALE_INDEX), outputEncoding, null);
    }

    /**
     * Gets a decoder.
     *
     * @return A decoder
     */
    private IDecoder getDecoder() {
        return ((IDecoder) subfilter.getEncoderManager().getEncoder());
    }

    public static TextUnitEventSplitter createTextUnitEventSplitter(IFilter subfilter, int subfilterIndex, LocaleId sourceLocale, List<LocaleId> targetLocales, String outputEncoding) {
        if (targetLocales.size() > 1) {
            throw new UnsupportedOperationException("Multiple locales are not supported in Subfilter");
        }
        return new TextUnitEventSplitter(subfilter, subfilterIndex, sourceLocale, targetLocales, outputEncoding);
    }
}

