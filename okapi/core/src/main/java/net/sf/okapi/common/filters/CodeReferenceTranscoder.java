package net.sf.okapi.common.filters;

import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.TextFragment;

import java.util.List;

import static net.sf.okapi.common.resource.TextFragment.TagType.CLOSING;
import static net.sf.okapi.common.resource.TextFragment.TagType.OPENING;
import static net.sf.okapi.common.resource.TextFragment.TagType.PLACEHOLDER;

/**
 * Provides a code reference transcoder.
 */
public class CodeReferenceTranscoder {

    private static final String UNEXPECTED_STRUCTURE = "Unexpected structure";

    private static final char MASKED_BASE_START = '\uE000';
    private static final char MASKED_BASE_END = '\uE100';

    private static final int INITIAL_CODE_ID = 1;
    private static final int ABSENT_CODE_PAIR_INDEX = -1;

    /**
     * Transcodes a coded text to the masked base.
     *
     * @param codedText A coded text
     *
     * @return A transcoded text
     */
    public static String transcodeToMaskedBase(String codedText) {
        StringBuilder stringBuilder = new StringBuilder();

        char[] chars = codedText.toCharArray();

        for (int i = 0; i < chars.length; i++) {

            if (!TextFragment.isMarker(chars[i])) {
                stringBuilder.append(chars[i]);
                continue;
            }

            int originalCodeIndex = TextFragment.toIndex(chars[++i]);
            stringBuilder.append(toMaskedCodeReference(originalCodeIndex));
        }

        return stringBuilder.toString();
    }

    /**
     * Transcodes a coded text to the masked base.
     *
     * @param codedText     A coded text
     * @param currentCodes  Current codes
     * @param originalCodes Original codes before the transcoding
     *
     * @return A text fragment with a transcoded text
     */
    public static TextFragment transcodeToMaskedBase(String codedText, List<Code> currentCodes, List<Code> originalCodes) {
        TextFragment textFragment = new TextFragment();

        int maskedCodeReferenceIndex = 0;

        char[] buf = codedText.toCharArray();

        for (int i = 0; i < buf.length; i++) {

            if (!TextFragment.isMarker(buf[i])) {
                textFragment.append(buf[i]);
                continue;
            }

            int codeIndex = TextFragment.toIndex(buf[++i]);

            if (!isCodeOriginal(originalCodes, currentCodes.get(codeIndex))) {
                textFragment.append(currentCodes.get(codeIndex));
                continue;
            }

            textFragment.append(toMaskedCodeReference(maskedCodeReferenceIndex++));
        }

        return textFragment;
    }

    /**
     * Checks whether the code is original.
     *
     * It does not take into account the {@code Code.type}.
     *
     * @param originalCodes Original codes to look through
     * @param currentCode   The current code to compare with
     *
     * @return {@code true}  if the current code is contained in the originals
     *         {@code false} otherwise
     */
    private static boolean isCodeOriginal(List<Code> originalCodes, Code currentCode) {

        for (Code originalCode : originalCodes) {
            if (originalCode.getId() == currentCode.getId()
                    && originalCode.getTagType() == currentCode.getTagType()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Transcodes a coded text from the masked base.
     *
     * @param codedText     A coded text
     * @param currentCodes  Current codes
     * @param originalCodes Original codes before the transcoding
     *
     * @return A text fragment with a transcoded text
     */
    public static TextFragment transcodeFromMaskedBase(String codedText, List<Code> currentCodes, List<Code> originalCodes) {
        TextFragment textFragment = new TextFragment();

        char chars[] = codedText.toCharArray();

        int currentCodeId = INITIAL_CODE_ID;

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];

            if (TextFragment.isMarker(c)) {
                int codeIndex = TextFragment.toIndex(chars[++i]);

                currentCodeId = alignCodeIdsAndTagTypes(currentCodes, codeIndex, currentCodeId);
                textFragment.append(currentCodes.get(codeIndex));

            } else if (isMaskedCodeReference(c)) {
                int codeIndex = fromMaskedCodeReference(c);

                currentCodeId = alignCodeIdsAndTagTypes(originalCodes, codeIndex, currentCodeId);
                textFragment.append(originalCodes.get(codeIndex));

            } else {
                textFragment.append(c);
            }
        }

        alignCodePairIdsAndTagTypes(textFragment.getCodes(), currentCodes);

        return renewTextFragment(textFragment);
    }

    private static int alignCodeIdsAndTagTypes(List<Code> codes, int codeIndex, int codeId) {
        Code code = codes.get(codeIndex);

        if (PLACEHOLDER == code.getTagType()) {
            code.setId(codeId);
            return getNextCodeId(codeId);
        }

        int codePairIndex;

        if (CLOSING == code.getTagType()) {
            codePairIndex = getOpeningCodePairIndex(codes, codeIndex);

            if (ABSENT_CODE_PAIR_INDEX == codePairIndex) {
                // there is no opening pair, converting to placeholder
                convertToPlaceholder(code);
                code.setId(codeId);

                return getNextCodeId(codeId);
            }

            return codeId;
        }

        // OPENING
        codePairIndex = getClosingCodePairIndex(codes, codeIndex);

        if (ABSENT_CODE_PAIR_INDEX == codePairIndex) {
            // there is no closing pair, converting to placeholder
            convertToPlaceholder(code);
            code.setId(codeId);

            return getNextCodeId(codeId);
        }

        code.setId(codeId);
        codes.get(codePairIndex).setId(codeId);

        return getNextCodeId(codeId);
    }

    private static int getNextCodeId(int currentCodeId) {
        return currentCodeId + 1;
    }

    private static int getOpeningCodePairIndex(List<Code> codes, int codeIndex) {
        Code code = codes.get(codeIndex);

        for (int i = 0; i < codeIndex; i++) {
            Code otherCode = codes.get(i);

            if (OPENING == otherCode.getTagType() && otherCode.getId() == code.getId()) {
                return i;
            }
        }

        return ABSENT_CODE_PAIR_INDEX;
    }

    private static int getClosingCodePairIndex(List<Code> codes, int codeIndex) {
        Code code = codes.get(codeIndex);

        for (int i = codeIndex + 1; i < codes.size(); i++) {
            Code otherCode = codes.get(i);

            if (CLOSING == otherCode.getTagType() && otherCode.getId() == code.getId()) {
                return i;
            }
        }

        return ABSENT_CODE_PAIR_INDEX;
    }

    private static void convertToPlaceholder(Code code) {
        code.setTagType(PLACEHOLDER);
    }

    private static void alignCodePairIdsAndTagTypes(List<Code> codes, List<Code> originalCodes) {

        for (Code originalCode : originalCodes) {
            if (PLACEHOLDER == originalCode.getTagType() || CLOSING == originalCode.getTagType()) {
                continue;
            }

            int codeIndex = getCodeIndexByCode(codes, originalCode);
            int codePairIndex = getClosingCodePairIndex(codes, codeIndex);

            if (isCrossStructurePair(codes, codeIndex, codePairIndex)) {
                convertToPlaceholder(originalCode);
                convertToPlaceholder(codes.get(codePairIndex));
                alignCodeIds(codes, codeIndex);
            }
        }
    }

    private static void alignCodeIds(List<Code> codes, int codeIndex) {
        int currentCodeId = codes.get(codeIndex).getId();

        for (int i = codeIndex + 1; i < codes.size(); i++) {
            Code code = codes.get(i);

            if (PLACEHOLDER == code.getTagType()) {
                code.setId(++currentCodeId);
                continue;
            }

            if (CLOSING == code.getTagType()) {
                continue;
            }

            // OPENING
            code.setId(++currentCodeId);

            int codePairIndex = getClosingCodePairIndex(codes, i);
            codes.get(codePairIndex).setId(currentCodeId);
        }
    }

    private static int getCodeIndexByCode(List<Code> codes, Code code) {
        for (int i = 0; i < codes.size(); i++) {
            if (code == codes.get(i)) {
                // this must be the same instance
                return i;
            }
        }

        throw new IllegalStateException(UNEXPECTED_STRUCTURE);
    }

    private static boolean isCrossStructurePair(List<Code> codes, int openingCodePairIndex, int closingCodePairIndex) {
        int minimumCodePairIndicesDifference = 1;

        if (minimumCodePairIndicesDifference == closingCodePairIndex - openingCodePairIndex) {
            // codes are going one by another
            return false;
        }

        for (int i = openingCodePairIndex + 1; i < closingCodePairIndex; i++) {
            Code innerCode = codes.get(i);

            if (PLACEHOLDER == innerCode.getTagType()) {
                continue;
            }

            int innerCodePairIndex;

            if (CLOSING == innerCode.getTagType()) {
                innerCodePairIndex = getOpeningCodePairIndex(codes, i);

                if (innerCodePairIndex < openingCodePairIndex) {
                    // the looked-at pair is wrapping the opening tag
                    return true;
                }
                continue;
            }

            // OPENING
            innerCodePairIndex = getClosingCodePairIndex(codes, i);

            if (innerCodePairIndex > closingCodePairIndex) {
                // the looked-at pair is wrapping the closing tag
                return true;
            }
        }

        return false;
    }

    private static TextFragment renewTextFragment(TextFragment textFragment) {
        TextFragment newTextFragment = new TextFragment();

        List<Code> codes = textFragment.getCodes();
        char chars[] = textFragment.getCodedText().toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];

            if (TextFragment.isMarker(c)) {
                int codeIndex = TextFragment.toIndex(chars[++i]);
                newTextFragment.append(codes.get(codeIndex));
            } else {
                newTextFragment.append(c);
            }
        }

        return newTextFragment;
    }

    /**
     * Transcodes a coded text from the masked base.
     *
     * @param codedText     A coded text
     * @param originalCodes Original codes
     *
     * @return A text fragment with a transcoded text
     */
    public static TextFragment transcodeFromMaskedBase(String codedText, List<Code> originalCodes) {
        TextFragment textFragment = new TextFragment();

        int currentCodeId = INITIAL_CODE_ID;

        for (char c : codedText.toCharArray()) {

            if (!isMaskedCodeReference(c)) {
                textFragment.append(c);
                continue;
            }

            int codeIndex = fromMaskedCodeReference(c);

            currentCodeId = alignCodeIdsAndTagTypes(originalCodes, codeIndex, currentCodeId);
            textFragment.append(originalCodes.get(codeIndex));
        }

        return textFragment;
    }

    private static boolean isMaskedCodeReference(char c) {
        return c >= MASKED_BASE_START && c <= MASKED_BASE_END;
    }

    private static char toMaskedCodeReference(int originalCodeIndex) {
        return (char) ((int) MASKED_BASE_START + originalCodeIndex);
    }

    private static int fromMaskedCodeReference(char c) {
        return ((int) c - (int) MASKED_BASE_START);
    }
}
