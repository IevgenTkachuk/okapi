package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides a text unit event holder.
 */
public class TextUnitEventHolder {

    private static final int EVENT_INDEX = 0;

    protected List<Event> events;

    public TextUnitEventHolder() {
        events = new ArrayList<>();
    }

    public TextUnitEventHolder(TextUnitEventHolder textUnitEventHolder) {
        events = new ArrayList<>(textUnitEventHolder.events);
    }

    public Event getAddedTextUnitEvent() {
        if (events.isEmpty()) {
            throw new IllegalStateException();
        }

        return events.get(EVENT_INDEX);
    }

    public TextUnitEventHolder addEvent(Event event) {
        events.add(EVENT_INDEX, event);

        return this;
    }

    public long textUnitCount(){
        return events.stream().filter(Event::isTextUnit).count();
    }
}
