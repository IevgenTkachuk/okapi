package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.resource.MultiEvent;

import java.util.Iterator;
import java.util.List;

import static net.sf.okapi.common.EventType.END_SUBFILTER;
import static net.sf.okapi.common.EventType.START_SUBFILTER;

/**
 * Provides a text unit event holder factory.
 */
public class TextUnitEventHolderFactory {

    public static TextUnitEventMerger createTextUnitEventMerger(MultiEvent event) {

        Iterator<Event> eventsIterator = event.iterator();

        Event startSubfilterEvent = eventsIterator.next();

        if (null == startSubfilterEvent || START_SUBFILTER != startSubfilterEvent.getEventType()) {
            throw new IllegalArgumentException();
        }

        TextUnitEventMerger textUnitEventMerger = new TextUnitEventMerger(
                startSubfilterEvent.getStartSubfilter().getParentId(),
                startSubfilterEvent.getStartSubfilter().getParentName(),
                startSubfilterEvent.getStartSubfilter().getParentSkeleton(),
                startSubfilterEvent.getStartSubfilter().getParentCodes(),
                startSubfilterEvent.getStartSubfilter().getSkeletonWriter(),
                startSubfilterEvent.getStartSubfilter().getDecoder()
        );

        while (eventsIterator.hasNext()) {
            Event currentEvent = eventsIterator.next();

            if (END_SUBFILTER == currentEvent.getEventType()) {
                return textUnitEventMerger;
            }

            textUnitEventMerger.addEvent(currentEvent);
        }

        throw new IllegalArgumentException();
    }

    public static TextUnitEventMerger createTextUnitEventMerger(TextUnitEventMerger originalMerger, List<Event> textUnitEvents) {
        return new TextUnitEventMerger(originalMerger).replaceAll(textUnitEvents);
    }

    public static TextUnitEventHolder createTextUnitEventHolder(Event event) {
        return new TextUnitEventHolder().addEvent(event);
    }
}
