package net.sf.okapi.lib.beans.v2;

import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.encoder.IDecoder;
import net.sf.okapi.common.filters.SubFilterSkeletonWriter;
import net.sf.okapi.common.resource.BaseNameable;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.StartSubfilter;
import net.sf.okapi.lib.beans.v1.CodeBean;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.beans.FactoryBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides a start subfilter bean.
 */
public class StartSubfilterBean extends net.sf.okapi.lib.beans.v1.StartSubfilterBean {

    private FactoryBean skelWriterBean = new FactoryBean();
    private FactoryBean decoderBean = new FactoryBean();

    private String parentName;
    private FactoryBean parentSkeletonBean = new FactoryBean();
    private List<CodeBean> parentCodeBeans;

    @Override
    protected StartSubfilter createObject(IPersistenceSession session) {
        StartSubfilter startSubfilter = super.createObject(session);

        startSubfilter.setSkelWriter(skelWriterBean.get(SubFilterSkeletonWriter.class, session));
        startSubfilter.setDecoder(decoderBean.get(IDecoder.class, session));
        startSubfilter.setParentName(parentName);
        startSubfilter.setParentSkeleton(parentSkeletonBean.get(ISkeleton.class, session));
        startSubfilter.setParentCodes(getCodes(parentCodeBeans, session));

        return startSubfilter;
    }

    private List<Code> getCodes(List<CodeBean> codeBeans, IPersistenceSession session) {
        if (null == codeBeans) {
            return null;
        }

        List<Code> codes = new ArrayList<>(codeBeans.size());

        for (CodeBean codeBean : codeBeans) {
            codes.add(codeBean.get(Code.class, session));
        }

        return codes;
    }

    @Override
    protected void setObject(BaseNameable obj, IPersistenceSession session) {
        super.setObject(obj, session);
    }

    @Override
    protected void fromObject(BaseNameable obj, IPersistenceSession session) {
        super.fromObject(obj, session);

        if (obj instanceof StartSubfilter) {
            StartSubfilter startSubfilter = (StartSubfilter) obj;

            skelWriterBean.set(startSubfilter.getSkeletonWriter(), session);
            decoderBean.set(startSubfilter.getDecoder(), session);

            parentName = startSubfilter.getParentName();
            parentSkeletonBean.set(startSubfilter.getParentSkeleton(), session);
            parentCodeBeans = getCodeBeans(startSubfilter.getParentCodes(), session);
        }
    }

    private List<CodeBean> getCodeBeans(List<Code> codes, IPersistenceSession session) {
        if (null == codes) {
            return null;
        }

        List<CodeBean> codeBeans = new ArrayList<>(codes.size());

        for (Code code : codes) {
            CodeBean codeBean = new CodeBean();
            codeBean.set(code, session);
            codeBeans.add(codeBean);
        }

        return codeBeans;
    }

    public FactoryBean getSkelWriterBean() {
        return skelWriterBean;
    }

    public void setSkelWriterBean(FactoryBean skelWriterBean) {
        this.skelWriterBean = skelWriterBean;
    }

    public FactoryBean getDecoderBean() {
        return decoderBean;
    }

    public void setDecoderBean(FactoryBean decoderBean) {
        this.decoderBean = decoderBean;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public FactoryBean getParentSkeletonBean() {
        return parentSkeletonBean;
    }

    public void setParentSkeletonBean(FactoryBean parentSkeletonBean) {
        this.parentSkeletonBean = parentSkeletonBean;
    }

    public List<CodeBean> getParentCodeBeans() {
        return parentCodeBeans;
    }

    public void setParentCodeBeans(List<CodeBean> parentCodeBeans) {
        this.parentCodeBeans = parentCodeBeans;
    }
}
