package net.sf.okapi.lib.beans.v2;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.resource.BaseNameable;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.beans.FactoryBean;

/**
 * Provides a start document bean.
 */
public class StartDocumentBean extends net.sf.okapi.lib.beans.v1.StartDocumentBean {

    private FactoryBean subfilterParametersBean = new FactoryBean();

    @Override
    protected BaseNameable createObject(IPersistenceSession session) {
        return super.createObject(session);
    }

    @Override
    protected void fromObject(BaseNameable obj, IPersistenceSession session) {
        super.fromObject(obj, session);

        if (obj instanceof StartDocument) {
            StartDocument startDocument = (StartDocument) obj;
            subfilterParametersBean.set(startDocument.getFilterParameters(), session);
        }
    }

    @Override
    protected void setObject(BaseNameable obj, IPersistenceSession session) {
        super.setObject(obj, session);

        if (obj instanceof StartDocument) {
            StartDocument startDocument = (StartDocument) obj;
            startDocument.setFilterParameters(subfilterParametersBean.get(IParameters.class, session));
        }
    }

    public FactoryBean getSubfilterParametersBean() {
        return subfilterParametersBean;
    }

    public void setSubfilterParametersBean(FactoryBean subfilterParametersBean) {
        this.subfilterParametersBean = subfilterParametersBean;
    }
}
