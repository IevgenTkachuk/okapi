package net.sf.okapi.subfiltering;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.filters.TextUnitEventSplitter;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.filters.html.HtmlFilter;

import java.util.List;

import static net.sf.okapi.common.filters.TextUnitEventSplitter.INITIAL_SECTION_INDEX;
import static net.sf.okapi.filters.html.HtmlFilter.HTML_FILTER_NAME;

@UsingParameters(StringParameters.class)
public class SubfilteringStep extends BasePipelineStep {

    private static final String DEFAULT_SUBFILTER_NAME = HTML_FILTER_NAME;

    private IFilterConfigurationMapper filterConfigurationMapper;
    private String subfilterConfigurationId = DEFAULT_SUBFILTER_NAME;
    private IFilter subfilter;
    private LocaleId sourceLocale;
    private List<LocaleId> targetLocales;
    private String outputEncoding;

    private int subFilterIndex = INITIAL_SECTION_INDEX;

    public SubfilteringStep(IFilter filter) {
        if (!(filter instanceof HtmlFilter)) {
            throw new UnsupportedOperationException("Only HtmlFilter is supported as Subfilter");
        }
        this.subfilter = filter;
    }

    public String getName() {
        return "Apply Sub-filtering";
    }

    public String getDescription() {
        return "Apply the specified filter to existing text units.";
    }

    @StepParameterMapping(parameterType = StepParameterType.FILTER_CONFIGURATION_MAPPER)
    public void setFilterConfigurationMapper(IFilterConfigurationMapper filterConfigurationMapper) {
        this.filterConfigurationMapper = filterConfigurationMapper;
    }

    @StepParameterMapping(parameterType = StepParameterType.SUBFILTER_CONFIGURATION_ID)
    public void setSubfilterConfigurationId(String subfilterConfigurationId) {
        this.subfilterConfigurationId = subfilterConfigurationId;
    }

    @StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
    public void setSourceLocale(LocaleId sourceLocale) {
        this.sourceLocale = sourceLocale;
    }

    @StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALES)
    public void setTargetLocales(List<LocaleId> targetLocales) {
        this.targetLocales = targetLocales;
    }

    @StepParameterMapping(parameterType = StepParameterType.OUTPUT_ENCODING)
    public void setOutputEncoding(String outputEncoding) {
        this.outputEncoding = outputEncoding;
    }

    @Override
    protected Event handleStartDocument(Event event) {

        if (null == filterConfigurationMapper) {
            return event;
        }

        subfilter = filterConfigurationMapper.createFilter(getSubfilterConfigurationId(), subfilter);

        subfilter.open(new RawDocument("", sourceLocale));
        StartDocument sd = event.getStartDocument();
        sd.setSubFilterConfig(getSubfilterConfigurationId());
        sd.setSubfilterParameters(subfilter.getParameters());
        subfilter.close();

        return event;
    }

    private String getSubfilterConfigurationId() {
        if (subfilterConfigurationId == null) {
            subfilterConfigurationId = DEFAULT_SUBFILTER_NAME;
        }
        return subfilterConfigurationId;
    }

    @Override
    protected Event handleTextUnit(Event event) {
        if (null == subfilter) {
            return event;
        }

        TextUnitEventSplitter splitter = TextUnitEventSplitter.createTextUnitEventSplitter(subfilter, subFilterIndex++,
                                                sourceLocale, targetLocales, outputEncoding);
        return new Event(EventType.MULTI_EVENT, splitter.split(event.getTextUnit()));
    }
}
