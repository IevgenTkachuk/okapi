package net.sf.okapi.subfiltering;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.filters.TextUnitEventMerger;
import net.sf.okapi.common.pipeline.BasePipelineStep;

@UsingParameters(StringParameters.class)
public class SubfilteringEventsMergeStep extends BasePipelineStep {

    private TextUnitEventMerger textUnitEventMerger;

    public String getName() {
        return "Merge Sub-filtering events";
    }

    public String getDescription() {
        return "Merge the previously specified filter events to parent filter events.";
    }

    @Override
    protected Event handleDocumentPart(Event event) {
        if (null == textUnitEventMerger) {
            return event;
        }

        textUnitEventMerger.addEvent(event);

        return Event.NOOP_EVENT;
    }

    @Override
    protected Event handleTextUnit(Event event) {
        return handleDocumentPart(event);
    }

    @Override
    protected Event handleStartSubfilter(Event event) {
        textUnitEventMerger = new TextUnitEventMerger(
                event.getStartSubfilter().getParentId(),
                event.getStartSubfilter().getParentName(),
                event.getStartSubfilter().getParentSkeleton(),
                event.getStartSubfilter().getParentCodes(),
                event.getStartSubfilter().getSkeletonWriter(),
                event.getStartSubfilter().getDecoder()
        );

        return Event.NOOP_EVENT;
    }

    @Override
    protected Event handleEndSubfilter(Event event) {
        Event textUnitEvent = textUnitEventMerger.merge();
        textUnitEventMerger = null;

        return textUnitEvent;
    }

}
