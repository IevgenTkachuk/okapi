package net.sf.okapi.subfiltering;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.IFileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.ZipFileCompare;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.openoffice.OpenOfficeFilter;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import net.sf.okapi.filters.openxml.OpenXMLPackageCompare;
import net.sf.okapi.filters.plaintext.PlainTextFilter;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatch;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatchItem;
import net.sf.okapi.lib.extra.pipelinebuilder.XParameter;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipeline;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipelineStep;
import net.sf.okapi.steps.common.FilterEventsToRawDocumentStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.rainbowkit.creation.ExtractionStep;
import net.sf.okapi.steps.rainbowkit.postprocess.MergingStep;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static net.sf.okapi.filters.html.HtmlFilter.HTML_FILTER_NAME;
import static net.sf.okapi.filters.openoffice.OpenOfficeFilter.OPEN_OFFICE_FILTER_NAME;
import static net.sf.okapi.filters.openxml.OpenXMLFilter.OPEN_XML_FILTER_NAME;
import static net.sf.okapi.filters.plaintext.PlainTextFilter.PLAIN_TEXT_FILTER_NAME;
import static net.sf.okapi.filters.rainbowkit.Manifest.MANIFEST_EXTENSION;
import static net.sf.okapi.filters.rainbowkit.Manifest.MANIFEST_FILENAME;
import static net.sf.okapi.filters.rainbowkit.RainbowKitFilter.RAINBOWKIT_PACKAGE_EXTENSION;
import static org.hamcrest.CoreMatchers.is;

@RunWith(DataProviderRunner.class)
public class SubfilteringStepTest {

    private static final String OUTPUT_DIRECTORY_NAME = "out";
    private static final String GOLD_DIRECTORY_NAME = "gold";

    private static final String XLIFF_FILE_EXTENSION = ".xlf";

//    private static final String XLIFF_KIT_EXTENSION = ".xkp";

    private static final String WORK_DIRECTORY_NAME = "work";
    private static final String TARGET_DIRECTORY_NAME = "done";

    private Path inputDirectoryPath;
    private Path outputDirectoryPath;
    private Path goldDirectoryPath;

    @Before
    public void initialize() throws URISyntaxException {
        Path workingDirectoryPath = Paths.get(getClass().getResource("/Subfiltering.odt").toURI()).getParent();

        inputDirectoryPath = workingDirectoryPath;
        outputDirectoryPath = workingDirectoryPath.resolve(OUTPUT_DIRECTORY_NAME);
        goldDirectoryPath = workingDirectoryPath.resolve(GOLD_DIRECTORY_NAME);
    }

    @DataProvider
    public static Object[][] roundTripDataProvider() {
        return new Object[][]{
                {"Subfiltering.odt", OPEN_OFFICE_FILTER_NAME, HTML_FILTER_NAME, new OpenOfficeFilter(), new HtmlFilter(), new ZipFileCompare(), true},
                {"Subfiltering.docx", OPEN_XML_FILTER_NAME, HTML_FILTER_NAME, new OpenXMLFilter(), new HtmlFilter(), new OpenXMLPackageCompare(), false},
                {"Subfiltering.xlsx", OPEN_XML_FILTER_NAME, HTML_FILTER_NAME, new OpenXMLFilter(), new HtmlFilter(), new OpenXMLPackageCompare(), true},
                {"Subfiltering.txt", PLAIN_TEXT_FILTER_NAME, HTML_FILTER_NAME, new PlainTextFilter(), new HtmlFilter(), new FileCompare(), true},

                {"2-paragraphs.odt", OPEN_OFFICE_FILTER_NAME, HTML_FILTER_NAME, new OpenOfficeFilter(), new HtmlFilter(), new ZipFileCompare(), false},
                {"2-paragraphs.docx", OPEN_XML_FILTER_NAME, HTML_FILTER_NAME, new OpenXMLFilter(), new HtmlFilter(), new OpenXMLPackageCompare(), false},
                {"2-paragraphs.txt", PLAIN_TEXT_FILTER_NAME, HTML_FILTER_NAME, new PlainTextFilter(), new HtmlFilter(), new FileCompare(), false},
        };
    }

    @Test
    @UseDataProvider("roundTripDataProvider")
    public void roundTrip(String filename, String filterConfigurationId, String subfilterConfigurationId,
                          IFilter filter, IFilter subfilter, IFileCompare fileCompare, boolean provideTranslation) throws Exception {
        Path goldFilePath = goldDirectoryPath.resolve(filename);
        Path outputFilePath = outputDirectoryPath.resolve(filename);

        FilterEventsToRawDocumentStep filterEventsToRawDocumentStep = new FilterEventsToRawDocumentStep();
        filterEventsToRawDocumentStep.setOutputURI(outputFilePath.toUri());
        filterEventsToRawDocumentStep.setOutputEncoding(StandardCharsets.UTF_8.name());

        if (!provideTranslation) {
            new XPipeline(
                    "Round-trip through Events",
                    new XBatch(
                            new XBatchItem(
                                    inputDirectoryPath.resolve(filename).toUri(),
                                    StandardCharsets.UTF_8.name(),
                                    LocaleId.ENGLISH,
                                    LocaleId.FRENCH)
                    ),

                    new RawDocumentToFilterEventsStep(filter),
                    new SubfilteringStep(subfilter),
                    new SubfilteringEventsMergeStep(),
                    filterEventsToRawDocumentStep
            ).execute();

            Assert.assertThat(fileCompare.filesExactlyTheSame(outputFilePath.toString(), goldFilePath.toString()), is(true));
        }

        String rainbowKitName = filename + RAINBOWKIT_PACKAGE_EXTENSION;

        new XPipeline(
                "To Rainbow Kit",
                inputDirectoryPath.toString(),
                outputDirectoryPath.toString(),
                new XBatch(
                        new XBatchItem(
                                inputDirectoryPath.resolve(filename).toUri(),
                                StandardCharsets.UTF_8.name(),
                                filterConfigurationId,
                                subfilterConfigurationId,
                                outputFilePath.toUri(),
                                StandardCharsets.UTF_8.name(),
                                LocaleId.ENGLISH,
                                LocaleId.FRENCH
                        )
                ),

                new RawDocumentToFilterEventsStep(),
                new SubfilteringStep(subfilter),
                new XPipelineStep(
                        new ExtractionStep(),
                        new XParameter("packageDirectory", outputDirectoryPath.toString()),
                        new XParameter("packageName", rainbowKitName)
//                        new XParameter("createZip", true)
                )
        ).execute();

        if (provideTranslation) {
            String workingFile = filename + XLIFF_FILE_EXTENSION;

            Files.copy(inputDirectoryPath.resolve(workingFile),
                    outputDirectoryPath.resolve(rainbowKitName).resolve(WORK_DIRECTORY_NAME).resolve(workingFile), REPLACE_EXISTING);
        }

        new XPipeline(
                "From Rainbow Kit",
                new XBatch(
                        new XBatchItem(
                                outputDirectoryPath.resolve(rainbowKitName).resolve(MANIFEST_FILENAME + MANIFEST_EXTENSION).toUri(),
//                                outputDirectoryPath.resolve(rainbowKitName).toUri(),
                                StandardCharsets.UTF_8.name(),
                                "okf_rainbowkit-noprompt",
                                outputFilePath.toUri(),
                                StandardCharsets.UTF_8.name(),
                                LocaleId.ENGLISH,
                                LocaleId.FRENCH
                        )
                ),

                new RawDocumentToFilterEventsStep(),
                new XPipelineStep(
                        new MergingStep(),
                        new XParameter("returnRawDocument", true)
                )
        ).execute();

        Assert.assertThat(fileCompare.filesExactlyTheSame(outputDirectoryPath.resolve(rainbowKitName).resolve(TARGET_DIRECTORY_NAME).resolve(filename).toString(),
                goldFilePath.toString()), is(true));

//        new XPipeline(
//                "To XLIFF Kit",
//                new XBatch(
//                        new XBatchItem(
//                                inputDirectoryPath.resolve(filename).toUri(),
//                                StandardCharsets.UTF_8.name(),
//                                LocaleId.ENGLISH, LocaleId.FRENCH)
//                ),
//
//                new RawDocumentToFilterEventsStep(filter),
//                new SubfilteringStep(subfilter),
//                new XPipelineStep(
//                        new XLIFFKitWriterStep(),
//                        new XParameter("gMode", true),
//                        new XParameter("includeOriginal", false),
//                        new XParameter("outputURI", outputDirectoryPath.resolve(filename + XLIFF_KIT_EXTENSION).toUri().toString()))
//        ).execute();

//        new XPipeline(
//                "From XLIFF Kit",
//                new XBatch(
//                        new XBatchItem(
//                                outputDirectoryPath.resolve(filename + XLIFF_KIT_EXTENSION).toUri(),
//                                StandardCharsets.UTF_8.name(),
//                                outputDirectoryPath.resolve(filename + XLIFF_KIT_EXTENSION + "." + OUTPUT_DIRECTORY_NAME).toUri(),
//                                StandardCharsets.UTF_8.name(),
//                                LocaleId.ENGLISH,
//                                LocaleId.FRENCH
//                        )
//                ),
//
//                new XLIFFKitReaderStep(),
//                new SubfilteringEventsMergeStep(),
//                filterEventsToRawDocumentStep
//        ).execute();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testJsonIsNotSupportedAsSubliter(){
        IFilter jsonFilter = new JSONFilter();
        SubfilteringStep subfilteringStep = new SubfilteringStep(jsonFilter);
    }
}
