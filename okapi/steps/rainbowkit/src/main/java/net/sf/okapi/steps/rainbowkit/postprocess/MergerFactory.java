package net.sf.okapi.steps.rainbowkit.postprocess;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.filters.rainbowkit.Manifest;
import net.sf.okapi.filters.rainbowkit.MergingInfo;
import net.sf.okapi.lib.tkit.filter.BeanEventFilter;

import static net.sf.okapi.filters.rainbowkit.Manifest.EXTRACTIONTYPE_ONTRAM;

/**
 * Provides a merger factory.
 */
class MergerFactory {

    /**
     * Creates a merger.
     *
     * @param filterConfigurationMapper The filter configuration mapper
     * @param parameters                The parameters
     * @param manifest                  The manifest
     * @param mergingInfo               The merging info
     * @param sourceLocale              The source locale
     * @param targetLocale              The target locale
     * @param isMultilingual            Is multilingual
     * @return A created merger
     */
    static IMerger createMerger(IFilterConfigurationMapper filterConfigurationMapper,
                                Parameters parameters,
                                Manifest manifest,
                                MergingInfo mergingInfo,
                                LocaleId sourceLocale,
                                LocaleId targetLocale,
                                boolean isMultilingual) {

        LocaleId targetLocaleToUse = getLocale(parameters, manifest, mergingInfo, targetLocale);

        if (mergingInfo.getUseSkeleton()) {
            return new SkeletonMerger(new BeanEventFilter(), parameters, manifest, mergingInfo, sourceLocale, targetLocaleToUse, isMultilingual);
        }

        IFilter filter = getFilter(filterConfigurationMapper, mergingInfo);
        IFilter subfilter = getSubfilter(filterConfigurationMapper, mergingInfo);

        return new Merger(filter, parameters, subfilter, manifest, mergingInfo, sourceLocale, targetLocaleToUse, isMultilingual);
    }

    private static LocaleId getLocale(Parameters parameters, Manifest manifest, MergingInfo mergingInfo, LocaleId targetLocale) {

        return parameters.getForceTargetLocale() || EXTRACTIONTYPE_ONTRAM.equals(mergingInfo.getExtractionType())
                ? targetLocale
                : manifest.getTargetLocale();
    }

    private static IFilter getFilter(IFilterConfigurationMapper filterConfigurationMapper, MergingInfo mergingInfo) {
        return getFilter(filterConfigurationMapper, mergingInfo.getFilterId(), mergingInfo.getFilterParameters());
    }

    private static IFilter getSubfilter(IFilterConfigurationMapper filterConfigurationMapper, MergingInfo mergingInfo) {
        return getFilter(filterConfigurationMapper, mergingInfo.getSubfilterId(), mergingInfo.getSubfilterParameters());
    }

    private static IFilter getFilter(IFilterConfigurationMapper filterConfigurationMapper, String filterConfigurationId, String filterParametersString) {
        IFilter filter = filterConfigurationMapper.createFilter(filterConfigurationId, null);

        if (null == filter) {
            return null;
        }

        IParameters filterParameters = filter.getParameters();

        if (filterParameters != null && filterParametersString != null) {
            filterParameters.fromString(filterParametersString);
        }

        return filter;
    }
}
