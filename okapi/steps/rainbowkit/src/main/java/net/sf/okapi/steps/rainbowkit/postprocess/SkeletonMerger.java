/*===========================================================================
  Copyright (C) 2013-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.postprocess;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiFileNotFoundException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.filters.rainbowkit.Manifest;
import net.sf.okapi.filters.rainbowkit.MergingInfo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Provides merging based on the skeleton file for a given extracted document (XLIFF).
 */
public class SkeletonMerger extends Merger {

    private static final String ERROR_MESSAGE = "Skeleton file not found.";

    /**
     * Constructs a merger object.
     *
     * @param filter         The filter to use
     * @param parameters     The parameters
     * @param manifest       The manifest to process
     * @param mergingInfo    The merging information
     * @param sourceLocale
     * @param targetLocale   The target locale
     * @param isMultilingual Is multilingual
     */
    public SkeletonMerger(IFilter filter,
                          Parameters parameters,
                          Manifest manifest,
                          MergingInfo mergingInfo,
                          LocaleId sourceLocale,
                          LocaleId targetLocale,
                          boolean isMultilingual) {
        super(filter, parameters, null, manifest, mergingInfo, sourceLocale, targetLocale, isMultilingual);
    }

    @Override
    protected void openOriginalResource() {
        String skeletonPath = manifest.getTempSkelDirectory() + mergingInfo.getRelativeInputPath() + ".skl";
        try {
            filter.open(new RawDocument(new FileInputStream(skeletonPath), null, targetLocale));
        } catch (FileNotFoundException e) {
            throw new OkapiFileNotFoundException(ERROR_MESSAGE, e);
        }
    }

    @Override
    protected IFilterWriter getWriter(StartDocument startDocument) {
        return startDocument.getFilterWriter();
    }

    @Override
    public Event mergeEvent(Event event) {
        switch (event.getEventType()) {

            case START_SUBDOCUMENT:
                if (parameters.getReturnRawDocument()) {
                    useSubDoc = true;
                }
                break;
            case END_SUBDOCUMENT:
                if (parameters.getReturnRawDocument()) {
                    flushFilterEvents();
                    return createMultiEvent();
                }
                break;
            default:
                return super.mergeEvent(event);
        }

        return event;
    }
}
