/*===========================================================================
  Copyright (C) 2011-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.postprocess;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.Range;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.TextUnitEventHolder;
import net.sf.okapi.common.filters.TextUnitEventMerger;
import net.sf.okapi.common.filters.TextUnitEventSplitter;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.filters.rainbowkit.Manifest;
import net.sf.okapi.filters.rainbowkit.MergingInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

import static net.sf.okapi.common.EventType.TEXT_UNIT;
import static net.sf.okapi.common.filters.TextUnitEventHolderFactory.createTextUnitEventHolder;
import static net.sf.okapi.common.filters.TextUnitEventHolderFactory.createTextUnitEventMerger;
import static net.sf.okapi.common.filters.TextUnitEventSplitter.INITIAL_SECTION_INDEX;

public class Merger implements IMerger {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	protected IFilter filter;
	protected Parameters parameters;
	protected IFilter subfilter;
	protected Manifest manifest;
	protected MergingInfo mergingInfo;
	protected LocaleId sourceLocale;
	protected LocaleId targetLocale;
	protected boolean isMultilingual;

	private IFilterWriter writer;
	private RawDocument rawDoc;

	private boolean skipEmptySourceEntries;
	private boolean useSource;
	private boolean forceSegmentationInMerge;
	protected boolean useSubDoc;

	protected int errorCount;
	private int subfilterIndex;

	private List<TextUnitEventHolder> textUnitEventHolder = new ArrayList<>();
	private List<Event> translationEvents = new LinkedList<>();

	/**
	 * Constructs a merger object.
	 *
	 * @param filter         The filter to use
	 * @param parameters     The parameters
	 * @param manifest       The manifest to process
	 * @param mergingInfo    The merging information
	 * @param sourceLocale   The source locale
	 * @param targetLocale   The target locale
	 * @param isMultilingual Is multilingual
	 */
	public Merger(IFilter filter,
				  Parameters parameters,
				  IFilter subfilter,
				  Manifest manifest,
				  MergingInfo mergingInfo,
				  LocaleId sourceLocale,
				  LocaleId targetLocale,
				  boolean isMultilingual) {
		this.filter = filter;
		this.parameters = parameters;
		this.subfilter = subfilter;
		this.manifest = manifest;
		this.mergingInfo = mergingInfo;
		this.sourceLocale = sourceLocale;
		this.targetLocale = targetLocale;
		this.isMultilingual = isMultilingual;
	}

	@Override
	public Event mergeStartDocumentEvent() {
		errorCount = 0;
		useSubDoc = false;
		logger.info("Merging: {}", mergingInfo.getRelativeInputPath());

		subfilterIndex = INITIAL_SECTION_INDEX;

		openOriginalResource();

		String outPath = getOutputPath(mergingInfo);

		// Skip entries with empty source for PO
		skipEmptySourceEntries = ( mergingInfo.getExtractionType().equals(Manifest.EXTRACTIONTYPE_PO)
			|| mergingInfo.getExtractionType().equals(Manifest.EXTRACTIONTYPE_TRANSIFEX)
			|| mergingInfo.getExtractionType().equals(Manifest.EXTRACTIONTYPE_TABLE) );
		// Use the source of the input as the translation for XINI, etc.
		useSource = mergingInfo.getExtractionType().equals(Manifest.EXTRACTIONTYPE_ONTRAM);

		// Process the start document in the document we just open
		Event internalEvent = filter.next();

		if (( internalEvent == null ) || ( internalEvent.getEventType() != EventType.START_DOCUMENT )) {
			errorCount++;
			logger.error("The start document event is missing when parsing the original resource.");
		}
		else {
			forceSegmentation(internalEvent.getStartDocument());

			writer = getWriter(internalEvent.getStartDocument());
			writer.setOptions(targetLocale, mergingInfo.getTargetEncoding());
			writer.setOutput(outPath);

			IParameters writerParameters = writer.getParameters();
			if ( writerParameters != null ) {
				writerParameters.fromString(mergingInfo.getFilterParameters());
			}

			writer.handleEvent(internalEvent);
		}

		// Compute what event to return
		if ( parameters.getReturnRawDocument() ) {
			if ( isMultilingual ) {
				rawDoc = new RawDocument(new File(outPath).toURI(), mergingInfo.getTargetEncoding(),
					manifest.getSourceLocale(), manifest.getTargetLocale());
			}
			else {
				// Otherwise: the previous target is now the source (and still the target)
				rawDoc = new RawDocument(new File(outPath).toURI(), mergingInfo.getTargetEncoding(),
					manifest.getTargetLocale(), manifest.getTargetLocale());
			}
			return Event.NOOP_EVENT;
		}
		else {
			return internalEvent;
		}
	}

	protected void openOriginalResource() {
		File file = new File(manifest.getTempOriginalDirectory() + mergingInfo.getRelativeInputPath());
		RawDocument rd = new RawDocument(file.toURI(), mergingInfo.getInputEncoding(),
				manifest.getSourceLocale(), targetLocale);
		filter.open(rd);
	}

	private String getOutputPath (MergingInfo mergingInfo) {
		if ( Util.isEmpty(parameters.getOverrideOutputPath()) ) {
			return manifest.getMergeDirectory() + mergingInfo.getRelativeTargetPath();
		}
		else {
			return Util.ensureSeparator(parameters.getOverrideOutputPath(), false) + mergingInfo.getRelativeTargetPath();
		}
	}

	private void forceSegmentation(StartDocument sd) {
		if (sd.getMimeType().equals(MimeTypeMapper.XLIFF_MIME_TYPE)) {
			net.sf.okapi.filters.xliff.Parameters prm = (net.sf.okapi.filters.xliff.Parameters) sd.getFilterParameters();
			forceSegmentationInMerge = (prm.getOutputSegmentationType()
					== net.sf.okapi.filters.xliff.Parameters.SegmentationType.SEGMENTED);
		} else {
			forceSegmentationInMerge = false;
		}
	}

	protected IFilterWriter getWriter(StartDocument startDocument) {
		return filter.createFilterWriter();
	}

	private void processTextUnit(Event event) {
		// Get the unit from the translation file
		ITextUnit traTu = event.getTextUnit();
		// If it's not translatable:
		if (!traTu.isTranslatable()) {
			return; // Do nothing: the original will be handled by processUntilTextUnit()
		}

		// search for the corresponding event in the original
		traTu.createTarget(targetLocale, false, IResource.COPY_ALL);

		// Store events until we have all of them
		translationEvents.add(event);

		TextUnitEventHolder originalTextUnitEventHolder = getOriginalTextUnitEventHolder();

		if (originalTextUnitEventHolder == null) {
			errorCount++;
			logger.error("No corresponding text unit for id='{}' in the original file.", traTu.getId());
			return;
		}
		// Process with merging as all events are in place
		if (originalTextUnitEventHolder.textUnitCount() == translationEvents.size()) {
			ITextUnit translatedTextUnit = null;
			Event oriEvent;
			if (originalTextUnitEventHolder instanceof TextUnitEventMerger) {
				translatedTextUnit = mergeSplitTextUnit((TextUnitEventMerger) originalTextUnitEventHolder, translationEvents);
				translatedTextUnit.createTarget(targetLocale, false, IResource.COPY_ALL);
				oriEvent = ((TextUnitEventMerger) originalTextUnitEventHolder).merge();
			} else {
				translatedTextUnit = event.getTextUnit();
				oriEvent = originalTextUnitEventHolder.getAddedTextUnitEvent();
			}

			// Get the actual text unit object of the original
			ITextUnit oriTu = oriEvent.getTextUnit();

			// Check the IDs
			if (!translatedTextUnit.getId().equals(oriTu.getId())) {
				errorCount++;
				logger.error("De-synchronized files: translated TU id='{}', Original TU id='{}'.",
						translatedTextUnit.getId(), oriTu.getId());
				return;
			}

			// Check if we have a translation
			TextContainer trgTraCont;
			if (useSource) trgTraCont = translatedTextUnit.getSource();
			else trgTraCont = translatedTextUnit.getTarget(targetLocale);

			if (trgTraCont == null) {
				if (oriTu.getSource().hasText()) {
					// Warn only if there source is not empty
					logger.warn("No translation found for TU id='{}'. Using source instead.", translatedTextUnit.getId());
				}
				writer.handleEvent(oriEvent); // Use the source
				return;
			}

			// Process the "approved" property
			boolean isTransApproved = false;
			Property traProp;
			if (useSource) traProp = translatedTextUnit.getSourceProperty(Property.APPROVED);
			else traProp = translatedTextUnit.getTargetProperty(targetLocale, Property.APPROVED);

			if (traProp != null) {
				isTransApproved = traProp.getValue().equals("yes");
			}
			if (!isTransApproved && manifest.getUseApprovedOnly()) {
				// Not approved: use the source
				logger.warn("Item id='{}': Target is not approved. Using source instead.", translatedTextUnit.getId());
				writer.handleEvent(oriEvent); // Use the source
				return;
			}

			// Do we need to preserve the segmentation for merging (e.g. TTX case)
			boolean mergeAsSegments = false;
			if (oriTu.getMimeType() != null) {
				if (oriTu.getMimeType().equals(MimeTypeMapper.TTX_MIME_TYPE)
						|| oriTu.getMimeType().equals(MimeTypeMapper.XLIFF_MIME_TYPE)) {
					mergeAsSegments = true;
				}
			}

			// Set the container for the source
			TextContainer srcOriCont = oriTu.getSource();
			TextContainer srcTraCont = translatedTextUnit.getSource();

			if (forceSegmentationInMerge) {
				// Use the source of the target to get the proper segmentations
				if (!srcOriCont.getUnSegmentedContentCopy().getCodedText().equals(
						srcTraCont.getUnSegmentedContentCopy().getCodedText())) {
					logger.warn("Item id='{}': Original source and source in the translated file are different.\n"
									+ "Cannot use the source of the translation as the new segmented source.",
							translatedTextUnit.getId());
				}
			}

			// If we do not need to merge segments then we must join all for the merge
			// We also remember the ranges to set them back after merging
			List<Range> srcRanges = null;
			List<Range> trgRanges = null;

			// Merge the segments together for the code transfer
			// This allows to move codes anywhere in the text unit, not just each part.
			// We do remember the ranges because some formats will required to be merged by segments
			if (!srcOriCont.contentIsOneSegment()) {
				if (mergeAsSegments) {
					// Make sure we remember the source segment if we merge as segments later
					// Otherwise the segment id of the source and target will defer
					srcRanges = srcOriCont.getSegments().getRanges();
				}
				srcOriCont.joinAll();
			}
			if (forceSegmentationInMerge) {
				// Get the source segmentation from the translated file if it's
				// a forced segmentation
				if (!srcTraCont.contentIsOneSegment()) {
					srcRanges = srcTraCont.getSegments().getRanges();
				}
			} else {
				// Else: take from the original source
				if (!srcOriCont.contentIsOneSegment()) {
					srcRanges = srcOriCont.getSegments().getRanges();
				}
			}
			if (!trgTraCont.contentIsOneSegment()) {
				trgRanges = trgTraCont.getSegments().getRanges();
				trgTraCont.joinAll();
			}

			// Perform the transfer of the inline codes
			// At this point we have a single segment/part
			TextUnitUtil.copySrcCodeDataToMatchingTrgCodes(srcOriCont.getFirstContent(),
					trgTraCont.getFirstContent(), true, true, null, oriTu);

			// Resegment for the special formats
			if (mergeAsSegments) {
				if (srcRanges != null) {
					srcOriCont.getSegments().create(srcRanges, true);
				}
				if (trgRanges != null) {
					trgTraCont.getSegments().create(trgRanges, true);
				}
			}

			// Check if the target has more segments
			if (srcOriCont.getSegments().count() < trgTraCont.getSegments().count()) {
				logger.warn("Item id='{}': There is at least one extra segment in the translation file.\n"
								+ "Extra segments are not merged into the translated output.",
						traTu.getId());
			}

			// Assign the translated target to the target text unit
			oriTu.setTarget(targetLocale, trgTraCont);

			// Update/add the 'approved' flag of the entry to merge if available
			// (for example to remove the fuzzy flag in POs or set the approved attribute in XLIFF)
			if (manifest.getUpdateApprovedFlag()) {
				Property oriProp = oriTu.createTargetProperty(targetLocale, Property.APPROVED, false, IResource.CREATE_EMPTY);
				if (traProp != null) {
					oriProp.setValue(traProp.getValue());
				} else {
					oriProp.setValue("yes");
				}
			}

			// Output the translation
			writer.handleEvent(oriEvent);

			// Set back the segmentation if we modified it for the merge
			// (Already done if it's a special format)
			if (parameters.getPreserveSegmentation() && !mergeAsSegments) {
				if (srcRanges != null) {
					srcOriCont.getSegments().create(srcRanges, true);
				}
				if (trgRanges != null) {
					trgTraCont.getSegments().create(trgRanges, true);
				}
			}
			   textUnitEventHolder.clear();
			   translationEvents.clear();
		}
	}

	private TextUnitEventHolder getOriginalTextUnitEventHolder() {
		TextUnitEventHolder originalTextUnitEventHolder;
		if (textUnitEventHolder.isEmpty()) {
			originalTextUnitEventHolder = processUntilTextUnit();
			textUnitEventHolder.add(originalTextUnitEventHolder);
		} else {
			originalTextUnitEventHolder = textUnitEventHolder.stream().findFirst().get();
		}
		return originalTextUnitEventHolder;
	}

	private ITextUnit mergeSplitTextUnit(TextUnitEventMerger originalTextUnitEventMerger, List<Event> textUnitEvent) {
		return createTextUnitEventMerger(originalTextUnitEventMerger, textUnitEvent).merge().getTextUnit();
	}

	/**
	 * Get events in the original document until the next text unit.
	 * Any event before is passed to the writer.
	 *
	 * @return the event of the next text unit, or null if no next text unit is found.
	 */
	private TextUnitEventHolder processUntilTextUnit() {
		while (filter.hasNext()) {
			Event event = filter.next();

			if (event.getEventType() != TEXT_UNIT) {
				writer.handleEvent(event);
				continue;
			}

			TextUnitEventHolder textUnitEventHolder;

			if (subfilter != null) {
				MultiEvent subfiltered = TextUnitEventSplitter.createTextUnitEventSplitter(subfilter, subfilterIndex, sourceLocale,
						Collections.singletonList(targetLocale), mergingInfo.getTargetEncoding())
						.split(event.getTextUnit());
				textUnitEventHolder = createTextUnitEventMerger(subfiltered);
			} else {
				textUnitEventHolder = createTextUnitEventHolder(event);
			}

			if (writeUntranslatableTextUnitEvent(textUnitEventHolder)) {
				continue;
			}

			return textUnitEventHolder;
		}

		// This text unit is extra in the translated file
		return null;
	}

	private boolean writeUntranslatableTextUnitEvent(TextUnitEventHolder textUnitEventHolder) {
		ITextUnit tu = textUnitEventHolder.getAddedTextUnitEvent().getTextUnit();

		if (!tu.isTranslatable()
				|| skipEmptySourceEntries && tu.isEmpty()) {
			// Do not merge the translation for non-translatable
			// For some types of package: Do not merge the translation for non-translatable
			writeFromTextUnitEventHolder(textUnitEventHolder);

			return true;
		}

		return false;
	}

	private void writeFromTextUnitEventHolder(TextUnitEventHolder textUnitEventHolder) {
		Event event = textUnitEventHolder instanceof TextUnitEventMerger
				? ((TextUnitEventMerger) textUnitEventHolder).merge()
				: textUnitEventHolder.getAddedTextUnitEvent();

		writer.handleEvent(event);
	}

	@Override
	public Event mergeEvent(Event event) {
		switch (event.getEventType()) {
			case TEXT_UNIT:
				processTextUnit(event);
				if (parameters.getReturnRawDocument()) {
					return Event.NOOP_EVENT;
				}
				break;
			case START_SUBDOCUMENT:
				if (parameters.getReturnRawDocument()) {
					useSubDoc = true;
					return Event.NOOP_EVENT;
				}
				break;
			case END_DOCUMENT:
				flushFilterEvents();
				closeWriterAndFilter();
				if (parameters.getReturnRawDocument() && !useSubDoc) {
					return createMultiEvent();
				}
				break;
			default:
				if (parameters.getReturnRawDocument()) {
					return Event.NOOP_EVENT;
				}
		}

		return event;
	}

	protected void flushFilterEvents() {
		while (filter.hasNext()) {
			writer.handleEvent(filter.next());
		}
	}

	private void closeWriterAndFilter() {
		if (writer != null) {
			writer.close();
			writer = null;
		}
		if (filter != null) {
			filter.close();
			filter = null;
		}
	}

	protected Event createMultiEvent () {
		List<Event> list = new ArrayList<>();

		// Change the pipeline parameters for the raw-document-related data
		PipelineParameters pp = new PipelineParameters();
		pp.setOutputURI(rawDoc.getInputURI()); // Use same name as this output for now
		pp.setSourceLocale(rawDoc.getSourceLocale());
		pp.setTargetLocale(rawDoc.getTargetLocale());
		pp.setOutputEncoding(rawDoc.getEncoding()); // Use same as the output document
		pp.setInputRawDocument(rawDoc);
		// Add the event to the list
		list.add(new Event(EventType.PIPELINE_PARAMETERS, pp));

		// Add raw-document related events
		list.add(new Event(EventType.RAW_DOCUMENT, rawDoc));

		// Return the list as a multiple-event event
		return new Event(EventType.MULTI_EVENT, new MultiEvent(list));
	}

	/**
	 * Gets the number of errors since the last call to {@link IMerger#mergeStartDocumentEvent()}.
	 * @return the number of errors.
	 */
	@Override
	public int getErrorCount () {
		return errorCount;
	}
}
