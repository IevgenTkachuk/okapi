/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.postprocess;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.filters.rainbowkit.Manifest;
import net.sf.okapi.filters.rainbowkit.MergingInfo;

import static net.sf.okapi.steps.rainbowkit.postprocess.MergerFactory.createMerger;

@UsingParameters(Parameters.class)
public class MergingStep extends BasePipelineStep {

	public static final String NAME = "Rainbow Translation Kit Merging";
	
	private Parameters params;
	private IFilterConfigurationMapper fcMapper;
	private LocaleId sourceLocale;
	private LocaleId targetLocale;

	private IMerger merger;

	public MergingStep () {
		super();
		params = new Parameters();
	}

	@Override
	public String getDescription () {
		return "Post-process a Rainbow translation kit."
			+ " Expects: filter events. Sends back: filter events or raw documents.";
	}

	@Override
	public String getName () {
		return NAME;
	}

	@StepParameterMapping(parameterType = StepParameterType.FILTER_CONFIGURATION_MAPPER)
	public void setFilterConfigurationMapper (IFilterConfigurationMapper fcMapper) {
		this.fcMapper = fcMapper;
	}
	
	public IFilterConfigurationMapper getFilterConfigurationMapper() {
		return fcMapper;
	}

	@StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
	public void setSourceLocale (LocaleId sourceLocale) {
		this.sourceLocale = sourceLocale;
	}

	@SuppressWarnings("deprecation")
	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
	public void setTargetLocale (LocaleId targetLocale) {
		this.targetLocale = targetLocale; 
	}
	
	public LocaleId getTargetLocale() {
		return targetLocale;
	}
	
	@Override
	public Event handleEvent (Event event) {
		switch ( event.getEventType() ) {
		case START_DOCUMENT:
			return handleStartDocument(event);
		default:
			if (merger != null) {
				return merger.mergeEvent(event);
			}
		}
		return event;
	}

	@Override
	protected Event handleStartDocument (Event event) {
		// Initial document is expected to be a manifest
		StartDocument sd = event.getStartDocument();

		MergingInfo mergingInfo = sd.getAnnotation(MergingInfo.class);
		if ( mergingInfo == null ) {
			throw new OkapiBadFilterInputException("Start document is missing the merging info annotation.");
		}

		Manifest manifest = sd.getAnnotation(Manifest.class);
		if ( manifest == null ) {
			throw new OkapiBadFilterInputException("Start document is missing the manifest annotation.");
		}

		merger = createMerger(fcMapper, params, manifest, mergingInfo, sourceLocale, targetLocale, sd.isMultilingual());

		return merger.mergeStartDocumentEvent();
	}

	@Override
	public IParameters getParameters () {
		return params;
	}
	
	@Override
	public void setParameters (IParameters params) {
		this.params = (Parameters)params;
	}

	protected int getErrorCount () {
		if ( merger != null ) return merger.getErrorCount();
		return -1;
	}

}
