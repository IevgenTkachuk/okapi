package net.sf.okapi.steps.rainbowkit.postprocess;

import net.sf.okapi.common.Event;

/**
 * Provides a merger interface.
 */
interface IMerger {

    Event mergeStartDocumentEvent();

    Event mergeEvent(Event event);

    // TODO: remove this method
    int getErrorCount();
}
